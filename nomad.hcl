job "postgresql" {
	datacenters = ["dc1"]
	type = "service"
	group "postgresql" {
		network {
			port "db" {
			  static = 5432
			  to = 5432
			}
		}
		service {
			name = "postgresql"
			tags = ["db"]
			port = "5432"
			check {
				type = "script"
				name = "Check PG ready"
				command = "su"
				args = ["postgres", "-c", "/usr/local/bin/pg_isready"]
				interval = "10s"
				timeout = "1s"
				task = "postgresql"
			}
		}
		task "postgresql" {
			resources {
				cpu = 2000
				memory = 200
			}
			vault {
				policies = ["postgresql"]
			}
			template {
				data = file("nomad/cachain.pem.tpl")
				destination = "local/cachain.pem"
			}
			template {
				data = file("nomad/cert.pem.tpl")
				destination = "local/cert.pem"
			}
			template {
				data = file("nomad/key.pem.tpl")
				destination = "secrets/key.pem"
				perms = 700
			}
			template {
				data = file("nomad/postgresql.conf")
				destination = "local/postgresql.conf"
			}
			template {
				data = file("nomad/file.env")
				destination = "secrets/file.env"
				env = true
			}
			template {
				data = file("nomad/fix-key-owner.sh")
				destination = "local/fix-key-owner.sh"
				perms = "755"
			}
			driver = "docker"
			config {
				image = "postgres:12-alpine"
				ports = ["db"]
				entrypoint = ["local/fix-key-owner.sh"]
				volumes = ["local/postgresql.conf:/var/lib/postgresql/data/postgresql.conf"]
			}
			volume_mount {
				volume = "data"
				destination = "/var/lib/postgresql/data"
			}
		}
		volume "data" {
			type = "host"
			source = "postgresql"
			read_only = false
		}
	}
}
