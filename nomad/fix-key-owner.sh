#!/bin/sh
cp /secrets/key.pem /tmp/key.pem
chown postgres.postgres /tmp/key.pem
chmod 600 /tmp/key.pem
docker-entrypoint.sh postgres
