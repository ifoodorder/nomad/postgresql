{{ with secret "pki_int/issue/cert" "role_name=postgresql" "common_name=postgresql.service.consul" "ttl=24h" "alt_names=_postgresql._tcp.service.consul,localhost" "ip_sans=127.0.0.1" }}
{{ .Data.certificate }}
{{ end }}
